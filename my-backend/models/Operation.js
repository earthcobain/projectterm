const mongoose = require('mongoose')
const Schema = mongoose.Schema

const operationSchema = new Schema(
  {
    date: {
      type: Date,
      required: true
    },
    type: {
      required: true,
      type: String,
      enum: ['Withdraw', 'Deposit']
    },
    amount: {
      required: true,
      type: Number
    }
  },
  { collection: 'Operations' }
)

module.exports = mongoose.model('Operation', operationSchema)
