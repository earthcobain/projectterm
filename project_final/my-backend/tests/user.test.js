const dbHandler = require('./db-handler')
const User = require('../models/User')
const Operation = require('../models/Operation')

beforeAll(async () => {
  await dbHandler.connect()
})

afterEach(async () => {
  await dbHandler.clearDatabase()
})

afterAll(async () => {
  await dbHandler.closeDatabase()
})

const userComplete = {
  name: 'Pianozaza',
  email: 'hi123@gmail.com',
  password: '1234567a'
}

const userInComplete1 = {
  name: '',
  email: 'hi123@gmail.com',
  password: '1234567a'
}

const userInComplete2 = {
  name: 'Pianozaza',
  email: 'hi123@gmail.com',
  password: ''
}

const userInComplete3 = {
  name: 'Pianozaza',
  email: '',
  password: '1234567a'
}

describe('User', () => {
  it('สามารถเพิ่ม user ได้ ', async () => {
    let error = null
    try {
      const user = new User(userComplete)
      await user.save()
      await user.generateAuthToken()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })

  it('ไม่สามารถเพิ่ม user ได้ เนื่องจากไม่มีชื่อ ', async () => {
    let error = null
    try {
      const user = new User(userInComplete1)
      await user.save()
      await user.generateAuthToken()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })

  it('ไม่สามารถเพิ่ม user ได้ เนื่องจากไม่มีพาสเวิร์ด ', async () => {
    let error = null
    try {
      const user = new User(userInComplete2)
      await user.save()
      await user.generateAuthToken()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })

  it('ไม่สามารถเพิ่ม user ได้ เนื่องจากไม่มีemail ', async () => {
    let error = null
    try {
      const user = new User(userInComplete3)
      await user.save()
      await user.generateAuthToken()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})

const operationComplete = {
  username: 'Pakawat',
  date: '2014-01-01T23:28:56.782Z',
  type: 'Deposit',
  amount: 1000
}
const operationInComplete1 = {
  username: 'Pakawat',
  date: '',
  type: 'Deposit',
  amount: 1000
}
const operationInComplete2 = {
  username: 'Pakawat',
  date: '2014-01-01T23:28:56.782Z',
  type: '',
  amount: 1000
}
const operationInComplete3 = {
  username: 'Pakawat',
  date: '2014-01-01T23:28:56.782Z',
  type: 'Deposit',
  amount: ''
}

const operationInComplete4 = {
  username: '',
  date: '2014-01-01T23:28:56.782Z',
  type: 'Deposit',
  amount: 1000
}

describe('Operation', () => {
  it('สามารถเพิ่ม รายการ ได้ ', async () => {
    let error = null
    try {
      const operation = new Operation(operationComplete)
      await operation.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('ไม่สามารถเพิ่ม รายการ ได้เนื่องจาก date ว่าง ', async () => {
    let error = null
    try {
      const operation = new Operation(operationInComplete1)
      await operation.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม รายการ ได้เนื่องจาก type ว่าง ', async () => {
    let error = null
    try {
      const operation = new Operation(operationInComplete2)
      await operation.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม รายการ ได้เนื่องจาก amount ว่าง ', async () => {
    let error = null
    try {
      const operation = new Operation(operationInComplete3)
      await operation.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม รายการ ได้เนื่องจาก user ว่าง ', async () => {
    let error = null
    try {
      const operation = new Operation(operationInComplete4)
      await operation.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
