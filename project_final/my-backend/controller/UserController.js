const User = require('../models/User')

exports.registerNewUser = async (req, res) => {
  try {
    const usersameemail = await User.find({ email: req.body.email })
    if (usersameemail.length >= 1) {
      return res.status(409).json({
        message: 'email already in use'
      })
    }
    const user = new User({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    })
    const data = await user.save()
    const token = await user.generateAuthToken() // here it is calling the method that we created in the model
    res.status(201).json({ data, token })
  } catch (err) {
    res.status(400).json({ err: err })
  }
}

exports.loginUser = async (req, res) => {
  try {
    const email = req.body.email
    const password = req.body.password
    const user = await User.findByCredentials(email, password)
    if (!user) {
      return res
        .status(401)
        .json({ error: 'Login failed! Check authentication credentials' })
    }
    const token = await user.generateAuthToken()
    res.status(201).json({ user, token })
  } catch (err) {
    res.status(400).json({ message: 'Login failed! Check Username or Password' })
  }
}

exports.getUserDetails = async (req, res) => {
  await res.json(req.userData)
}
