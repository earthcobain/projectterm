const express = require('express')
const router = express.Router()
const operationController = require('../controller/OperationController')

router.get('/:username', operationController.getOperations)

router.post('/', operationController.addOperation)

module.exports = router
