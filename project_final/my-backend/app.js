var express = require('express')
var cors = require('cors')
var path = require('path')
var cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
var logger = require('morgan')
const mongoose = require('mongoose')
mongoose.set('useCreateIndex', true)

mongoose.connect('mongodb://admin:123456@localhost/mydb', {
  useUnifiedTopology: true,
  useNewUrlParser: true
})

var operationsRouter = require('./routes/operations')
var usersRouter = require('./routes/users')
var indexRouter = require('./routes/index')

var app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/', indexRouter)
app.use('/user', usersRouter)
app.use('/operations', operationsRouter)

module.exports = app
